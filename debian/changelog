libjson-maybexs-perl (1.004008-1+apertis0) apertis; urgency=medium

  * Sync from debian/trixie.

 -- Apertis CI <devel@lists.apertis.org>  Sat, 08 Mar 2025 05:20:22 +0000

libjson-maybexs-perl (1.004008-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.004008.
  * Update years of upstream copyright.
  * Declare compliance with Debian Policy 4.7.0.

 -- gregor herrmann <gregoa@debian.org>  Sun, 11 Aug 2024 02:26:59 +0200

libjson-maybexs-perl (1.004005-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.004005.
  * Update years of upstream copyright.
  * Declare compliance with Debian Policy 4.6.2.
  * Drop unneeded version constraints from (build) dependencies.

 -- gregor herrmann <gregoa@debian.org>  Sun, 25 Jun 2023 04:44:10 +0200

libjson-maybexs-perl (1.004004-1+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Sat, 01 Apr 2023 02:58:25 +0000

libjson-maybexs-perl (1.004004-1) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster
    * Build-Depends-Indep: Drop versioned constraint on libjson-xs-perl.
    * libjson-maybexs-perl: Drop versioned constraint on libjson-xs-perl
      in Depends.

  [ gregor herrmann ]
  * Import upstream version 1.004004.
  * Update years of upstream copyright.
  * Declare compliance with Debian Policy 4.6.1.

 -- gregor herrmann <gregoa@debian.org>  Wed, 21 Sep 2022 19:06:13 +0200

libjson-maybexs-perl (1.004003-1apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 11 Mar 2021 15:21:31 +0000

libjson-maybexs-perl (1.004003-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.004003.
  * Declare compliance with Debian Policy 4.5.1.

 -- gregor herrmann <gregoa@debian.org>  Fri, 20 Nov 2020 17:48:32 +0100

libjson-maybexs-perl (1.004002-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.004002.
  * Add new test dependency.
  * Bump debhelper-compat to 13.

 -- gregor herrmann <gregoa@debian.org>  Thu, 21 May 2020 20:03:44 +0200

libjson-maybexs-perl (1.004001-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

  [ gregor herrmann ]
  * Import upstream version 1.004001.
  * Update years of upstream copyright.
  * Update {alternative,versioned} (build) dependencies.
  * Declare compliance with Debian Policy 4.5.0.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.

 -- gregor herrmann <gregoa@debian.org>  Sat, 02 May 2020 22:47:00 +0200

libjson-maybexs-perl (1.004000-1co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Sat, 20 Feb 2021 01:11:28 +0000

libjson-maybexs-perl (1.004000-1) unstable; urgency=medium

  * Import upstream version 1.004000
  * Update copyright years
  * Declare compliance with Debian Policy 4.1.4

 -- Florian Schlichting <fsfs@debian.org>  Mon, 14 May 2018 21:32:21 +0200

libjson-maybexs-perl (1.003010-1) unstable; urgency=medium

  * Team upload.

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Import upstream version 1.003010.
  * debian/copyright: add info about new third-party file.
  * Update upstream contact information.
  * Bump debhelper compatibility level to 10.

 -- gregor herrmann <gregoa@debian.org>  Mon, 19 Mar 2018 20:22:31 +0100

libjson-maybexs-perl (1.003009-1) unstable; urgency=medium

  * Import upstream version 1.003009
  * Declare compliance with Debian Policy 4.1.0

 -- Florian Schlichting <fsfs@debian.org>  Sun, 24 Sep 2017 14:01:51 +0200

libjson-maybexs-perl (1.003008-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.003008.
  * Update (build) dependencies.

 -- gregor herrmann <gregoa@debian.org>  Sun, 30 Oct 2016 15:41:29 +0100

libjson-maybexs-perl (1.003007-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Nuno Carvalho ]
  * Import upstream version 1.003007.
  * debian/control:
    + update standards version to 3.9.8.
    + drop libtest-without-module-perl dependency, no longer required.
    + update libjson-pp-perl required version.
    + update perl required version of dual life package, per cme suggestion.
  * debian/copyright: add Karen Etheridge as upstream contact.
  * debian/upstream/metadata: add Karen Etheridge as contact.

  [ gregor herrmann ]
  * Update repository URLs in debian/upstream/metadata.
  * Bump debhelper compatibility level to 9.

 -- Nuno Carvalho <smash@cpan.org>  Wed, 14 Sep 2016 15:01:28 +0100

libjson-maybexs-perl (1.003005-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.003005
  * Update upstream copyright holders.

 -- gregor herrmann <gregoa@debian.org>  Sat, 27 Jun 2015 23:20:51 +0200

libjson-maybexs-perl (1.002006-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.002006

 -- gregor herrmann <gregoa@debian.org>  Wed, 22 Oct 2014 20:14:53 +0200

libjson-maybexs-perl (1.002005-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Add debian/upstream/metadata
  * Import upstream version 1.002005
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.

 -- gregor herrmann <gregoa@debian.org>  Mon, 13 Oct 2014 20:45:14 +0200

libjson-maybexs-perl (1.002002-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Update long description from upstream's module description.
  * Update (build) dependencies.

 -- gregor herrmann <gregoa@debian.org>  Tue, 06 May 2014 20:09:27 +0200

libjson-maybexs-perl (1.001000-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Wed, 25 Dec 2013 19:29:18 +0100

libjson-maybexs-perl (1.000000-1) unstable; urgency=low

  * Initial Release. (Closes: #730578)

 -- Florian Schlichting <fsfs@debian.org>  Tue, 26 Nov 2013 21:08:12 +0100
